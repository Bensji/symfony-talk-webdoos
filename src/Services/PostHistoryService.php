<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Post;
use App\Entity\PostHistory;
use App\Exceptions\BlogException;
use App\Manager\PostHistoryManager;
use App\Repository\PostHistoryRepository;

class PostHistoryService
{
    private $repository;
    private $manager;
    private $cacheService;

    public function __construct(
        PostHistoryRepository $repository,
        PostHistoryManager $manager,
        CacheService $cacheService
    ) {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->cacheService = $cacheService;
    }

    public function insert(Post $post)
    {
        $postHistory = new PostHistory();
        $postHistory->setPost($post);

        try {
            $this->manager->save($postHistory);
            $this->cacheService->invalidateKey(PostHistory::class);
        } catch (\Exception $exception) {
            new BlogException($exception->getMessage());
        }
    }

    public function getViewCount(Post $post)
    {
        $parameterBag = [
            "postId" => $post->getId(),
        ];

        return $this->manager->getViewCount($parameterBag);
    }
}