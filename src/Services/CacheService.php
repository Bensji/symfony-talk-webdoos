<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\RedisCacheException;
use Predis\ClientInterface;

class CacheService
{
    private $redisClient;

    function __construct(ClientInterface $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    public function get(string $key)
    {
        try {
            return $this->redisClient->get($key);
        } catch (\Exception $exception) {
            new RedisCacheException($exception->getMessage());
        }
    }

    public function save(string $key, $value)
    {
        $value = $this->serialize($value);

        try {
            return $this->redisClient->set($key, $value);
        } catch (\Exception $exception) {
            new RedisCacheException($exception->getMessage());
        }
    }

    public function invalidateKey(string $key)
    {
        try {
            return $this->redisClient->del($key);
        } catch (\Exception $exception) {
            new RedisCacheException($exception->getMessage());
        }
    }

    private function serialize($value): string
    {
        return json_encode($value);
    }
}