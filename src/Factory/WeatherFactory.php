<?php

namespace App\Factory;

use App\Model\BadWeather;
use App\Model\GoodWeather;
use App\Model\WeatherResponse;
use Symfony\Component\Config\Definition\Exception\Exception;

class WeatherFactory
{
    public static function create(WeatherResponse $weather)
    {
        switch ($weather->getWeatherType()) {
            case WeatherResponse::GOOD_WEATHER:
                return new GoodWeather($weather);
            case WeatherResponse::BAD_WEATHER:
                return new BadWeather($weather);
            default:
                new Exception("Case not found.");
        }
    }
}