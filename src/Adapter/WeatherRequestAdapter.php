<?php

namespace App\Adapter;

use App\Exceptions\WeatherException;
use App\Model\WeatherResponse;
use GuzzleHttp\Client;
use Symfony\Component\Config\Definition\Exception\Exception;

class WeatherRequestAdapter
{
    const WEATHER_URL = "https://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22";

    public function fetchWeatherData(): WeatherResponse
    {
        $client = new Client();
        $request = $client->get(self::WEATHER_URL);

        if ($request->getStatusCode() === 200) {
            return $this->adaptWeatherData($request->getBody()->getContents());
        }

        $message = sprintf("Error while fetching the Error data %d", $request->getReasonPhrase());
        new WeatherException($message);
    }

    private function adaptWeatherData($response): WeatherResponse
    {
        $response = \GuzzleHttp\json_decode($response);

        try {
            return new WeatherResponse($response);
        } catch (Exception $exception) {
            new WeatherException($exception->getMessage());
        }
    }
}