<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;

abstract class AbstractRepository extends ServiceEntityRepository implements RepositoryInterface
{
    protected $entityClass;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, $this->entityClass);
    }

    /** @inheritdoc */
    public function getAll(array $parameterBag): ?array
    {
        $qb = $this->getFilterQuery($parameterBag);

        return $qb->getQuery()->getResult();
    }

    /** @inheritdoc */
    public function getOne(array $parameterBag)
    {
        $qb = $this->getFilterQuery($parameterBag);

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            /** @TODO > 1 result throws this exception. Do something with it. */
        }
    }

    /** @inheritdoc */
    public function getFilterQuery(array $parameterBag): QueryBuilder
    {
        $qb = $this
            ->createQueryBuilder('s');

        if (isset($parameterBag['id'])) {
            $qb->andWhere('s.id=(:id)')
                ->setParameter('id', $parameterBag['id']);
        }

        return $qb;
    }
}