<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\QueryBuilder;

interface RepositoryInterface
{
    public function getAll(array $parameterBag): ?array;
    public function getOne(array $parameterBag);
    public function getFilterQuery(array $parameterBag): QueryBuilder;
}