<?php

namespace App\Repository;

use App\Entity\PostHistory;

class PostHistoryRepository extends AbstractRepository
{
    protected $entityClass = PostHistory::class;

    /** @inheritdoc */
    public function getViewCount($parameterBag)
    {
        $qb = $this->getFilterQuery($parameterBag);

        if (isset($parameterBag['postId'])) {
            $qb->andWhere('s.post=(:post)')
                ->setParameter('post', $parameterBag['postId']);
        }

        $qb->select('count(s.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }
}