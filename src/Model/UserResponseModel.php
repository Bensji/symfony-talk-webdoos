<?php

namespace App\Model;

use App\Entity\User;

class UserResponseModel
{
    public $myAwesomeUserName;
    public $thisIsSomethingElse;

    function __construct(User $user)
    {
        $this->myAwesomeUserName = $this->setUserName($user->getFullName());
        $this->thisIsSomethingElse = $user->getEmail();
    }

    public function setUserName(string $fullName): string
    {
        return html_entity_decode($fullName);
    }
}