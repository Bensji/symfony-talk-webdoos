<?php

namespace App\Model;

class WeatherResponse
{
    const GOOD_WEATHER = "Sunny";
    const BAD_WEATHER = "Cloudy";

    public $weather;
    public $clouds;
    public $wind;
    public $base;
    public $name;

    function __construct($response)
    {
        $this->weather = $response->weather;
        $this->clouds = $response->clouds;
        $this->wind = $response->wind;
        $this->base = $response->base;
        $this->name = $response->name;
    }

    public function isTshirtWeather()
    {

    }

    public function getWeatherType()
    {
        return self::GOOD_WEATHER;
    }
}