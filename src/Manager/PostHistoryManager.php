<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\PostHistory;
use App\Repository\PostHistoryRepository;
use App\Services\CacheService;
use Doctrine\ORM\EntityManagerInterface;

class PostHistoryManager extends AbstractManager
{
    /**
     * @var PostHistoryRepository
     */
    protected $repository;
    private $cacheService;

    public function __construct(EntityManagerInterface $manager, PostHistoryRepository $repository, CacheService $cacheService)
    {
        parent::__construct($manager, $repository);

        $this->cacheService = $cacheService;
    }

    public function save(PostHistory $postHistory)
    {
        $this->manager->persist($postHistory);
        $this->manager->flush();
    }

    public function getViewCount($parameterBag)
    {
        $cacheViewCount = $this->getCacheViewCount();

        if (!$cacheViewCount) {
            $viewCount = $this->repository->getViewCount($parameterBag);
            $this->cacheService->save(PostHistory::class, $viewCount);

            return $viewCount;
        }

        return $cacheViewCount;
    }

    private function getCacheViewCount()
    {
        return $this->cacheService->get(PostHistory::class);
    }
}