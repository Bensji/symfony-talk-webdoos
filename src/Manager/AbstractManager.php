<?php

declare(strict_types=1);

namespace App\Manager;

use App\Repository\RepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractManager
{
    protected $manager;
    protected $repository;

    public function __construct(
        EntityManagerInterface $manager,
        RepositoryInterface $repository
    )
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    public function getAll($parameterBag = []): ?array
    {
        return $this->repository->getAll($parameterBag);
    }

    public function getOne($parameterBag = [])
    {
        return $this->repository->getOne($parameterBag);
    }
}