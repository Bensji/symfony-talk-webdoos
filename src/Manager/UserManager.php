<?php

declare(strict_types=1);

namespace App\Manager;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserManager extends AbstractManager
{
    /**
     * @var UserRepository
     */
    protected $repository;

    public function __construct(EntityManagerInterface $manager, UserRepository $repository)
    {
        parent::__construct($manager, $repository);
    }
}