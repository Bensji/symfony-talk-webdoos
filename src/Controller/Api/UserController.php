<?php

namespace App\Controller\Api;

use App\Manager\UserManager;
use App\Model\UserResponseModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route()
 */
class UserController extends AbstractController
{
    private $userManager;

    function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @Route("/users", defaults={"_format"="json"}, methods={"GET"}, name="get_users")
     */
    public function getUsers()
    {
        $users = $this->userManager->getAll();

        $usersResponse = [];

        foreach ($users as $user) {
            $usersResponse[] = new UserResponseModel($user);
        }

        return new Response(\GuzzleHttp\json_encode($usersResponse));
    }
}