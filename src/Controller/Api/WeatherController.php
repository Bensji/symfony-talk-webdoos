<?php

namespace App\Controller\Api;

use App\Adapter\WeatherRequestAdapter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route()
 */
class WeatherController extends AbstractController
{
    private $weatherRequestAdapter;

    function __construct(WeatherRequestAdapter $weatherRequestAdapter)
    {
        $this->weatherRequestAdapter = $weatherRequestAdapter;
    }

    /**
     * @Route("/weather", defaults={"_format"="json"}, methods={"GET"}, name="get_weather")
     */
    public function index()
    {
        $weatherData = $this->weatherRequestAdapter->fetchWeatherData();

        return new Response(\GuzzleHttp\json_encode($weatherData));
    }
}