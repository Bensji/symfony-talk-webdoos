<?php

namespace App\Exceptions;

use Throwable;

class BlogException extends AbstractException
{
    const ERROR_CODE = 11;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = sprintf("Blog exception %s:", $message);

        parent::__construct($message, self::ERROR_CODE, $previous);
    }
}