<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

class RedisCacheException extends AbstractException
{
    const ERROR_CODE = 10;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = sprintf("Redis cache exception %s:", $message);

        parent::__construct($message, self::ERROR_CODE, $previous);
    }
}