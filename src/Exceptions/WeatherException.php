<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

class WeatherException extends AbstractException
{
    const ERROR_CODE = 20;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = sprintf("Weather request exception %s:", $message);

        parent::__construct($message, self::ERROR_CODE, $previous);
    }
}